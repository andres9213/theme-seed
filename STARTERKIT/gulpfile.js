'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var browserSync = require('browser-sync').create();
var globbing = require('gulp-css-globbing');
var autoprefixer = require('gulp-autoprefixer');
var cssImageDimensions = require('gulp-css-image-dimensions');
var greplace = require('gulp-replace');
var babel = require('gulp-babel');
var path = require('path');
var kss  = require('kss');

gulp.task('server-start', ['sass', 'js'], function(){
  gulp.watch(['source/sass/**.scss', 'source/sass/**/**.scss', 'source/js-ES6/*.js'], ['sass', 'js']);
  gulp.watch('**.html').on('change', browserSync.reload);
});

gulp.task('sass', function () {
    gulp.src(['source/sass/*/**.scss', 'source/sass/**.scss'])
      .pipe(plumber({
        errorHandler: function (error) {
          console.log(error.message);
          this.emit('end');
      }}))
      .pipe(globbing({
        extensions: ['.scss']
      }))
      .pipe(greplace(/^\s*(@import\s+)?url\((["'][^"'\)]+['"])(?:\))?(;)?\s*$/gm, '$1$2$3'))
      .pipe(sass({
        includePaths: [ './scss' ],
        errLogToConsole: true,
        outputStyle: 'compact'
      }))
      .pipe(autoprefixer({
        browsers: ['last 2 versions', 'ie >= 6', 'chrome >= 4', 'ff >= 3'],
        cascade: false
      }))
      .pipe(cssImageDimensions('../../images'))
      .pipe(gulp.dest('./css'))
      .pipe(browserSync.stream())
});

gulp.task('js', function() {
  gulp.src(['source/js-ES6/*.js'])
    .pipe(plumber({
        errorHandler: function (error) {
          console.log(error.message);
          this.emit('end');
    }}))
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(gulp.dest('./js'))
});

gulp.task('default',['server-start']);

//=======================================================
// Generate style guide
//=======================================================

gulp.task('styleguide', function() {
 console.log(__dirname);
 return kss({
   source: [
     __dirname + '/source/sass'
   ],
   destination: __dirname + '/source/styleguide',
   builder: __dirname + '/source/builder',
   namespace: 'seed_cappi:' + __dirname + '/source/sass',
   'extend-drupal8': true,
   // The css and js paths are URLs, like '/misc/jquery.js'.
   // The following paths are relative to the generated style guide.
   css: [
     __dirname + '/source/styleguide/',
     __dirname + '/css/sumoselect.css',
     __dirname + '/css/base/base.css',
     __dirname + '/css/component/component.css',
   ],
   js: [
     __dirname + '/js/libs/jquery.sumoselect.min.js',
     __dirname + '/js/libs/style-guide.js'
   ],
   homepage: 'style-guide.md',
   title: 'Style Guide'
 });
});

